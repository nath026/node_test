//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const ListModelSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
});

// Compile model from schema
const List = mongoose.model('List', ListModelSchema );

module.exports = List;