//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const Schema = mongoose.Schema;

const TaskModelSchema = new Schema({
    listId: { type: Schema.Types.ObjectId, ref: 'List' },
    desc: String,
});

// Compile model from schema
const Task = mongoose.model('Task', TaskModelSchema );

module.exports = Task