const mongoose = require("mongoose");
const { MongoMemoryServer } = require("mongodb-memory-server");
const { MongoClient } = require("mongodb");

let mongoServer = MongoMemoryServer;

const connect = async () => {
    mongoServer = await MongoMemoryServer.create();
    connection = await MongoClient.connect(mongoServer.getUri(), {});
};

const close = async () => {
    await mongoose.connection.dropDatabase();
    console.log('droping connection')
    await mongoose.connection.close();
    console.log('droping close')
};

const clear = async () => {
    console.log('clear db')
    const collections = mongoose.connection.collections;
    for (const key in collections) {
        await collections[key].deleteMany({});
    }
};

module.exports = {
    connect,
    close,
    clear,
};