const db = require('../db/db.js');
const Task = db.Task


async function getById(id) {

    const task = await Task.findById(id).lean();
    return task;
}

async function getAll() {

    const tasks = await Task.find().lean();
    return tasks;
}

async function create({listId, desc}){
    const task = new Task({listId, desc})
    await task.save();
}

module.exports = {
    getById,
    getAll,
    create
};
