const db = require('../db/db.js');
const User = db.User
const bcrypt = require('bcryptjs');
const auth = require('../helpers/jwt.js')


async function login({ email, password }) {
    const user = await User.findOne({email}).lean();
    // synchronously compare user entered password with hashed password
    if(user && bcrypt.compareSync(password, user.password)){
        const token = auth.generateAccessToken(email);
        // call toJSON method applied during model instantiation
        return {...user, token}
    }
}

async function register(params){
    // instantiate a user modal and save to mongoDB
    if (await User.findOne({ email: params.email })) {
        throw 'email "' + params.email + '" is already taken';
    }
    const user = new User(params)
    await user.save();
}

async function getById(id) {

    const user = await User.findById(id).lean();
    // call toJSON method applied during model instantiation
    return user
}

async function getAll() {
    const users = await User.find().lean();
    // console.log(users)
    return users;
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}

module.exports = {
    login,
    register,
    getById,
    getAll,
    update,
    _delete
};