const db = require('../db/db.js');
const List = db.List

async function getById(id) {
    const list = await List.findById(id);
    // call toJSON method applied during model instantiation
    return user.toJSON()
}

async function getAll(id) {
    const list = await List.find().lean();
    // call toJSON method applied during model instantiation
    return list
}

async function create(params){
    // instantiate a user modal and save to mongoDB
    const list = new List(params)
    await list.save();
}

module.exports = {
    getById,
    getAll,
    create
};