// tests/product.test.js
const dbHandler = require('../lib/db-handler');
const supertest = require("supertest");
const app = require("../app.js");
const request = supertest(app);

let token = ""
let id = ""

describe("Users routes", () => {
  beforeAll(async () => await dbHandler.connect());
  afterEach(async () => await dbHandler.clear());
  afterAll(async () => await dbHandler.close());
  it("should return all users with empty array", async () => {
    
    const response = await request.get("/users").send();
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(0);
  });

  it("register a user", async () => {
    const response = await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("username", "fake");
  });

  it("login", async () => {
    const response = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("username", "fake");
  });
  // it("get user by id", async () => {
  //   const response = await request
  //     .post("/users/login")
  //     .set("Content-Type", "application/json")
  //     .send({
  //       email: 'fake@fake.fk',
  //       password: '1234567890',
  //     });
  // });


  it("sould return an user by Id", async () => {
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
     const responseLogin = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
      token = responseLogin.body.token
      id = responseLogin.body._id;
    const response = await request
      .get(`/users/${id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(response.status).toBe(200);
    expect(response.body._id).toBe(id);
  });

  it("sould return all users", async () => {
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake2',
        email: 'fake2@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
     const responseLogin = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
      token = responseLogin.body.token;
    const response = await request
      .get(`/users/`)
      .set('Authorization', `Bearer ${token}`);
      
    expect(response.status).toBe(200);
    expect(response.body.length).toBeGreaterThan(1);
  });

  // delete user
  it("should delete an user by Id", async () => {
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
     const responseLogin = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
      token = responseLogin.body.token
      id = responseLogin.body._id;
    const response1 = await request
      .delete(`/users/${id}`)
      .set('Authorization', `Bearer ${token}`);

    expect(response1.status).toBe(200);
  });

  // delete not found user
  it("should throw error id not found", async () => {
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
     const responseLogin = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
      token = responseLogin.body.token;
      // id = responseLogin.body._id;
    const response1 = await request
      .delete(`/users/blabla`)
      .set('Authorization', `Bearer ${token}`);

    expect(response1.status).toBe(500);
  });

  it("update user", async () => {
    await request
      .post("/users/register")
      .set("Content-Type", "application/json")
      .send({
        username: 'fake',
        email: 'fake@fake.fk',
        password: '1234567890',
        role: "ROLE_USER"
      });
    const responseLogin = await request
      .post("/users/login")
      .set("Content-Type", "application/json")
      .send({
        email: 'fake@fake.fk',
        password: '1234567890',
      });
      token = responseLogin.body.token
      id = responseLogin.body._id;
    await request
      .put(`/users/${id}`)
      .set('Authorization', `Bearer ${token}`)
      .send({username: 'fakefake'});
    
    const response = await request
      .get(`/users/${id}`)
      .set('Authorization', `Bearer ${token}`);
    expect(response.status).toBe(200);
    expect(response.body._id).toBe(id);
    expect(response.body.username).toBe('fakefake');

  });
  
});