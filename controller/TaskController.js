const express = require('express')
const router = express.Router();
const taskServices = require('../services/TaskService.js')

router.get('/', function (req, res, next) {
    taskServices.getAll()
    .then(tasks => res.json(tasks))
    .catch(err => next(err));
})

router.get('/:id', (req, res, next) => {
    taskServices.getById(req.params.id).then(
        (task) => res.json(task)
    ).catch(err => next(err))
})

router.post("/", async (req, res, next) => {
    const {listId, desc} = req.body
    taskServices.create({listId, desc})
    .then( res.json({success:true}))
    .catch(err => next(err))
})

router.delete('/:id', (req, res, next) => {
    taskServices.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
})

module.exports = router;