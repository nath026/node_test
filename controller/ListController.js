const express = require('express')
const router = express.Router();
const listServices = require('../services/ListService')

router.get('/', (req, res, next) => {
    listServices.getAll().then(
        (list) => res.json(list)
    ).catch(err => next(err))
})

router.get('/:id', (req, res, next) => {
    listServices.getById(req.params.id).then(
        (list) => res.json(list)
    ).catch(err => next(err))
})

router.post('/', (req, res, next) => {
    listServices.create(req.body).then(
        res.json({success:true})
    ).catch(err => next(err))
})

module.exports = router;