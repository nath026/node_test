const express = require('express')
const router = express.Router();
const bcrypt = require('bcryptjs')
const userServices = require('../services/UserService.js')

router.post('/register', (req, res, next) => {
    const {password} = req.body
    const salt = bcrypt.genSaltSync(10);
    req.body.password = bcrypt.hashSync(password, salt);

        userServices.register(req.body).then(
            res.status(201).json(req.body)
        ).catch(err => next(err))
})

router.post('/login', (req, res, next) => {
    const { email, password} = req.body;
    userServices.login({email, password})
    .then(user => user ? res.json(user) : res.status(400).json({ message: 'Email or password is incorrect' }))
    .catch(err => next(err));
})

router.get('/', (req, res, next) => {
    userServices.getAll()
    .then(users => res.json(users))
    .catch(err => next(err));
})

router.get('/:id', (req, res, next) => {
    userServices.getById(req.params.id).then(
        (user) => res.json(user)
    ).catch(err => next(err))
})

router.delete('/:id', (req, res, next) => {
    userServices._delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
})

router.put('/:id', (req, res, next) => {
    userServices.update(req.params.id, req.body)
    .then(() => res.json({message: "modification faite"}))
    .catch(err => next(err));
   })

module.exports = router;