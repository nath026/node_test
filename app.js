const express = require("express");
const cors = require('cors');
const unless = require('express-unless')
const auth = require('./helpers/jwt.js');
const users = require('./controller/UserController.js')
const task = require('./controller/TaskController.js')
const list = require('./controller/ListController.js')

const errors = require('./helpers/errorHandler.js')

const app = express();

app.use(cors()) // Default = CORS-enabled for all origins Access-Control-Allow-Origin: *!
app.use(express.json()) // middleware for parsing application/json
app.use(express.urlencoded({ extended: false })) // for parsing application/x-www-form-urlencoded

// middleware for authenticating token submitted with requests
auth.authenticateToken.unless = unless
app.use(auth.authenticateToken.unless({
    path: [
        { url: '/users/login', methods: ['POST']},
        { url: '/users/register', methods: ['POST']},
        { url: '/lists', methods: ['GET', 'POST']},
        { url: '/tasks', methods: ['GET', 'POST']},
        { url: '/', methods: ['GET']},
        { url: '/users', methods: ['GET']},

    ]
}))

app.get('/',(req, res) => {
  res.send('Hello World!');
  });

app.use('/users', users) // middleware for listening to routes
app.use('/tasks', task) // middleware for listening to routes
app.use('/lists', list) // middleware for listening to routes

app.use(errors.errorHandler); // middleware for error responses

module.exports = app;