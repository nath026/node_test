const mongoose = require('mongoose');
const config = require('../config.json');

mongoose
  .connect(process.env.MONGO_URL || config.connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('mongo connected'));

module.exports = mongoose.connection;