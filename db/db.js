const mongoose = require('./mongo')
const User =  require('../models/UserModel')
const Task =  require('../models/TaskModel')
const List =  require('../models/ListModel')

module.exports = {
    mongoose,
    User,
    Task,
    List
};